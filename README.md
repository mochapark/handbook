[![Netlify Status](https://api.netlify.com/api/v1/badges/e2ed183e-9a07-443c-8d07-e935f2bfa5cf/deploy-status)](https://app.netlify.com/sites/cranky-liskov-1a087f/deploys)

# Mocha Park Handbook

This repository contains the Mocha Park handbook. Both the repository and compiled handbook are available publicly.

The handbook documentation is built using VuePress. We encourage all Mocha Park team members to contribute to the handbook. If you're looking for help on how to contribute (especially for non-technical team members), reach out in the `#handbook` slack channel. It may also be helpful to read the [VuePress Getting Started Guide](https://vuepress.vuejs.org/guide/getting-started.html).

All other relevant information can be found in the handbook itself.

## Installation, Building, and Deployment

You'll need to have yarn installed. Once installed, there are two primary commands:

1. To build and start a development server, run `yarn handbook:dev`.
2. To build the docs into source files, run `yarn handbook:build`. This is not required to deploy the handbook.

Builds and deployments occur automatically with new commits or merges into the `master` branch on Bitbucket. Modifications should always be done in a non-master branch and pull requested when ready for deployment. There are not currently any CI/CD pipelines created for the the handbook.

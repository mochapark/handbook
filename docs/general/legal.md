# Legal

The handbook is not a legal document. If you have any legal questions feel free to send a message in the `#legal` slack channel. Employment and contract related questions may be more efficiently handled through the `#peopleops` channel.

Note, both of these channels are public to all team members so do not share any confidential or privileged information. You can email [legal@mochapark.com](mailto:legal@mochapark.com) for confidential matters.

## Contract Templates

Commonly used contract templates are stored on Google Drive. These contracts still require the same signing procedure below. This is because any contract is dangerous if the necessary team members don't know about it.

## Signing Legal Documents

Currently, due to our small size, an officer must sign any and all legal documents. Reach out in the `#legal` channel if you need a contract signed.

## Practical Implications of Employee and Contractor Relationships

While we generally try to hire team members as employees, there are circumstances, both inside and outside the U.S., where we hire team members on contract basis either directly or through a reseller. Due to these legal differences, we need to be sure not to refer to the whole team as employees.

For example, we won't host an "employee appreciation day", as we would have to exclude contractors for participating. However, we can (and should) host "team appreciation days".

It should be noted that the handbook is targeted towards full-time employees. Contractors should expect differences in their contract, and employees may have differences in their employment agreements (likely due to legal restrictions or requirements in your home country). Additionally, the handbook is not a legal document and your individual contract or employment agreement will take precedence over any items in the handbook.

For reference, there are a number of reasons to hire individuals on contract instead of as employees. For the sake of transparency, here are some key reasons:

* Faster Hiring: When looking to fill an immediate role, resellers provide a lot of value with pre-vetted candidates and the ability to rapidly engage.
* Short Term Engagements: Some roles are only needed for known and definite time frames. In many cases, it makes more sense for the team member and the company to work on a contract basis. Time frames may be defined as "until project X is finished, expected Y months" or just "for Z months".
* Legal challenges in the home country: It is difficult to hire overseas employees in some countries while simple to hire them as contractors. We look to hire the best talent, period; sometimes that means working in countries that do not have favorable laws for overseas companies to hire. In many of those cases we will offer contract employment.

## Other

When in doubt ask [legal@mochapark.com](mailto:legal@mochapark.com) for private communication and `#slack` for public communication.

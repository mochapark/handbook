## Acronyms & Terms

A repository of commonly used acronyms and terms.

* CDN - Content Delivery Network. A distributed network of servers for delivering content to our sites and applications.

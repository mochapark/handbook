# Equipment

For the sake of efficiency, we currently require team members to provide all "ordinary" equipment to complete their jobs. This primarily includes a computer and an internet connection but may also include items like pens, paper, and a desk (but all of that is up to you).

This policy may change in the future as we grow and it becomes more efficient to purchase and manage a supply of equipment. Until then, we find it important to be transparent and up front about this policy. Ensure you take this into account while you review and negotiate your compensation.

We will cover expenses outside of ordinary work equipment either through direct payment or reimbursement. Just ask for approval prior to purchase as we may have deals, points, or other promotions we can use.

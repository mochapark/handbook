# Mission

## **_To improve urban living by increasing infrastructure utilization through the sharing economy._**
---

### Background

This mission statement is an announcement of what team members in our company stand for, defining what we do, how we do what we do, and for whom we do what we do.

Our work is meaningful because of this mission. As you are regularly faced with options, primarily around where you spend your time, you may find it helpful to ask this question: **Is what I'm doing going to improve someone's life by increasing infrastructure utilization through the sharing economy?**

Phrasing the mission statement as a question may guide you in pursuing the most impactful work.

### Criticisms

One of most common criticisms of our mission is it's too technical and doesn't have the emotional hook of many mission statements. This is purposeful.

First, this mission statement isn't a marketing slogan, it's an internal statement for team members to use the same guiding light. Our mission statement is team member focused as it is able to shed the inefficiencies of serving multiple use cases.

Second, we are a technical team. We believe in solving peoples' problems through technology. This is an important mission; if you agree, consider joining us.

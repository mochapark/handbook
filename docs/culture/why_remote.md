## Why Remote?

Mocha Park is an all-remote, distributed team. We have no main office, no satellite offices, and encourage team members to work from wherever they want.

Why?

TL;DR - It's a win-win for team members and the company. Team members are happier and more productive. The company does well well team members are happy and productive.

### Freedom

We allow people to work when they want, where they want. This provides freedom to work when and how they see best. We think this fosters more productivity and overall happiness than instructing people where to work, when to work, and how to work.

### Larger Talent Pool

We have the opportunity to hire from (almost) anywhere in the world. This not only creates a greater talent pool, it offers inherent differences of perspectives, and a more interesting team to work with.

### Productivity

Where's the place you go to work when you need to be productive? It's unlikely you said in a crowded office between 10 AM and 3 PM. Wherever it is, we think our team members know best, and one size (one office) doesn't fit all.

### The Commute (or lack thereof)

We've known for a long time that commutes are damaging to our mental health. The average commute in the U.S. is nearly an hour each day. That's time you could have spent with your family, running errands, going to the bank, eating breakfast, cleaning, or even working.

Americans spend an average of [$2,600 in after tax money on their commute](https://www.cbsnews.com/news/americans-spend-2600-a-year-on-their-commutes/). It's a lose-lose-lose. The company pays for it, whether directly or indirectly in compensation, the team member has to spend the money, and the environmental impact is significant. All so that we can send a Slack message to each other from the same room.

No required commute means real money saved for our team members and real reductions in environmental impact. That makes us happy.

## Proposed Downsides

There are downsides to remote work. We try to minimize the downsides, but they exists regardless. Here are some of those downsides.

### Harder to Build Culture

This is perhaps the most challenging aspect of the remote team. It takes real human interaction to build trust and culture. We try to mitigate this by occasionally getting together in person, and video chats. Since we don't have the "advantage" of spontaneously running into each other in the halls, we have to consciously make time to chat.

### Reduced Spontaneous Communication

Spontaneous face-to-face communication is lower in an all remote environment. However, we think this enables purposeful, concise conversations.

### Less Accountability

If you care about time spent over deliverables, this may be true. We don't have any way of looking over the shoulder of all our team members. At the end of the day, either they meet their self defined objectives or they don't.

### Network Effects

It's true that there is huge value in being centrally located in hubs creates network effects. While we won't become fully engrained in a single hub, we have the advantage of having team members distributed through multiple hubs, in many ways expanding our reach.

### Tax Breaks from Cities

True, we won't get tax breaks from cities to move and hire people there because we don't plan on setting up an office is any city. Also, we don't much care for political engineering.

## Contributing to Culture

The words stated in this handbook are only meaningful if they are taken as a true reflection of the values of the team members. Everything from tonality to context to the literal words in the handbook should be a reflection of every team member.

It's important for team members to [contribute](https://bitbucket.org/mochapark/handbook/src) to all of those aspects of the handbook to help shape our culture.

The culture section of the handbook is designed to provide an overview of our self defined culture, but the rest of the content, and the actions of team members as a result of that content, is the true manifestation of our culture.

# Values

These are our company values that were set upon the founding of the company and have since changed to represent input from all of our team members. We seek to work with people that can add to these values, not just those that conform within them.

Our values can be summed up with a single phrase: ***Be Neighborly***. If we had an office, this would be the dominant phrase on the walls. This statement defines we we interact with customers, each other, and the world.

### Communication

We value **fast**, **frequent**, and **friendly** communication (the 3F's).

**Fast**: Be clear and concise, but no waste time over-editing. Communication is critical, but the act of creating communications, both written and verbal, should be minimized as it requires time otherwise dedicated to improving the product.

**Frequent**: There's a lot of flexibility working at Mocha Park, but team members need to be aware of your work, and when you're working. Going to take a vacation? Great, just make sure your team knows.

**Friendly**: Unfriendly work environments are counter productive. We're solving big problems and things are guaranteed to go wrong, which can cause stress. In order to maximize productivity and job satisfaction, it's important to actively aim for a friendly environment.

### Honesty & Trust

Honesty and trust together create a positive feedback loop. Communicate with honesty and trust that others will do the same.

### Humility

Communication amongst a team should never be sugar coated. Putting people in a mode of constantly pitching themselves and their work creates a culture that values showmanship over contributions. We value the work over the presentation, and therefore value humility; that is, the ability to reflect open and honestly.

In many cases, it is more impressive to know the shortcomings of your work than its strengths.


### Individuality

While throughout this handbook we use the word "we" to describe the company's view, we recognize our team members are individuals and don't agree with every statement made. We value individuals that openly share their opinions and uniqueness. That's why the handbook is a living document, as is expected to change with every new team member.

If you disagree with anything mentioned in the handbook, create an [issue](https://bitbucket.org/mochapark/handbook/issues) in the repository. Thanks to our values of communication, honesty, and humility, all team members' thoughts are encouraged to share their opinions and will be considered thoughtfully by other team members, regardless of current consensus.

// Reference here: https://stackoverflow.com/questions/52684077/how-to-add-vuetify-to-default-vuepress-theme
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faGlobeAmericas } from '@fortawesome/free-solid-svg-icons'
// import { farGlobe } from '@fortawesome/free-regular-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'


export default ({
  Vue, // the version of Vue being used in the VuePress app
  options, // the options for the root Vue instance
  router, // the router instance for the app
  siteData // site metadata
}) => {
  // apply enhancements here
  Vue.use(BootstrapVue)

  library.add(faGlobeAmericas, fab)
  Vue.component('font-awesome-icon', FontAwesomeIcon)
}

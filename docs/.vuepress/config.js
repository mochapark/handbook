require('dotenv').config()
const webpack = require('webpack')

module.exports = {
  title: 'Mocha Park Team Handbook',
  description: 'The documented guide to working at Mocha Park.',

  head: [
    ['link', { rel: 'icon', href: '/img/favicon.png' }],
    ['meta', { name: 'robots', content: 'noindex'}]
  ],
  themeConfig: {
    repo: 'https://bitbucket.org/mochapark/handbook', // location of repository on Bitbucket
    repoLabel: '', // Label in top right corner to link to the repo
    editLinks: true,
    editLinkText: '', // Default: Edit this page
    lastUpdated: 'Last Updated', // string | boolean, add UNIX timestamp of each file's last git commit and added to bottom of each page
    nav: [
    ],
    sidebar: [
      {
        title: 'General & Miscellaneous',
        collapsable: false,
        children: [
          ['/', 'Introduction'],
          ['/general/mission.md', 'Mission'],
          ['/general/equipment.md', 'Equipment'],
          ['/general/legal.md', 'Legal'],
          ['/general/acronyms.md', 'Acronyms']
        ]
      },
      {
        title: 'Culture',
        collapsable: false,
        children: [
          ['/culture/', 'Culture Summary'],
          ['/culture/values.md', 'Values'],
          ['/culture/why_remote.md', 'Why Remote?'],
          ['/culture/contributing_to_culture.md', 'Contributing to Culture']
        ]
      },
      {
        title: 'People Ops',
        collapsable: false,
        children: [
          ['/peopleops/', 'People Ops Summary'],
          ['/peopleops/team_members.md', 'Team Members'],
          ['/peopleops/side_projects.md', 'Working on Side Projects'],
          ['/peopleops/onboarding.md', 'Onboarding'],
          ['/peopleops/offboarding.md', 'Offboarding'],
          ['/peopleops/compensation.md', 'Compensation']
        ]
      },
      {
        title: 'Engineering',
        collapsable: false,
        children: [
          ['/engineering/', 'Summary'],
          ['/engineering/docs/', 'Documentation']
        ]
      },
    ]
  },

  configureWebpack: (config) => {
    return { plugins: [
      new webpack.EnvironmentPlugin({ ...process.env })
    ]}
  },

  plugins: [
    [
      '@vuepress/google-analytics',
      {
        'ga': 'UA-130125158-2'
      }
    ]
  ]
}

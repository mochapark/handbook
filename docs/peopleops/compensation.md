# Compensation & Benefits

We try to provide compensation that is both fair and a reflection of your contributions. While we don't publicize everyone's compensation, you're free to discuss among team members.

We want all team members to feel open to discuss compensation in order to provide a more efficient allocation of resources towards our mission.

As we grow, we expect the structure of our compensation to change, especially in regards to the benefits programs offered.

## Cash

Cash compensation may come in the form of employment or contractual engagements. While we do not publish or maintain a strict formula for calculating cash contributions, we try to maintain open and honest conversations with team members to provide mutually acceptable cash compensation.

## Benefits

As a small company, we have a lot of advantages in creating positive experiences for team members (see [our culture](/culture)). One of the disadvantages is the relatively high per capita cost of providing benefit programs. For that reason, you will likely find our benefits to be lacking compared to larger companies. Like everything else, we try to be very transparent so that you have access to all available information prior to making a decision about working with us.

Here's a summary of benefits we offer, benefits we plan to offer, and an explanation on some benefits we do not offer. This is a baseline guide for U.S. employees and may differ for employees outside the U.S. and contractors.

#### Remote Work <Badge text="In Place" type="success"/>

We are not just remote friendly, we are all remote. Every single person can work from wherever they please. So you never have to feel like you're "missing out" by being the one person working form home. More on this [here](/culture).

#### Unlimited Vacation <Badge text="In Place" type="success"/>

The unlimited vacation policy has received a bad rap recently as many employees found that despite the "unlimited" statement they were in practice, allotted very little vacation time. Despite this, we have decided to enact such a policy for two primary reasons:

1. It's what you do if you trust team members to deliver.
2. It's administratively preferable to tracking and reporting vacation time. Remember, we only care about your contributions, so when setting goals with your team, it's on you to determine what is reasonable to fit within your life with your expected vacation time.

No one is counting, but we currently believe that ~4 weeks of vacation provides sufficient time to recharge.

It is expected that you share you vacation plans with your team and not leave them hanging. In summary, be reasonable in taking time off and be reasonable in working with others to take time off.

#### Matching 401(k) <Badge text="Planned Offering" type="warn"/>

While this is not currently offered, it is likely to be one of the early benefits programs we adopt as it is tax favorable for both the team members and company. We do not currently offer a 401(k) or other retirement planning program due to cost prohibitions.

#### Health, Dental, Vision <Badge text="Planned Offering" type="warn"/>

We do not currently have a company health, dental, or vision insurance plan but it is a planned offering. In the meantime, we will help you identify private insurance plans that fit your needs.

## Equity

We provide at least partial compensation in equity to team members to provide pride of ownership and upside potential for long term commitments.

Unfortunately, many private companies have dramatically overpromised on the value of the equity they offer and are not transparent about how they would profit from the equity. This is primarily due to the failure to properly disclose the lack of liquidity, potential tax implications, and strict exercise windows upon leaving the company.

We are working on a program that avoids these pitfalls. Details on our equity package will be published soon.

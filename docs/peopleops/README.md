# People Ops

This is the section dedicated to People Ops. This includes information relevant to all team members, not just People Ops team members.

The slack channel, `#peopleops` is dedicated to supporting all team members. Specific People Ops projects should use alternative channels.

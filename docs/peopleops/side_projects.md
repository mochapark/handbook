# Working on Other Side Projects and Startups

This section generally applies to full time, salaried team members. Your individual contract may be slightly different, but here's our default position on team members working on outside projects.

**Summary:** We don't really care what you do with your personal time, we only care about results. So, whether you want to build your own startup, brew beer, hike, write a book, or anything else, we support team members enjoying life. There are three primary restrictions on this rule:

1. If you're working on another company, tell us about it. As long as you aren't working on a competing product, we'll set you up to make sure you own the IP.
2. You don't get to work on a competing business while working with us.
3. You can't take another corporate job.

Here are more details.

### Tell us about your new business

You don't have to tell us about your hobbies (of course), but if you plan to form a business, or have formed another business, we want, and think we have the right to know about it. Additionally, we think it's beneficial to everyone for a few reasons.

* We're more than happy to ensure you have the legal rights to your IP.
* If you're successful (and we hope you are), we're probably going to have to fill your position. The more advance notice, the better.
* We do want to be the judge of if this is a competing business. We promise to be reasonable in our analysis, we just ask that you're up front with the company.

### Not working on a competing business

For obvious reasons, we can't allow team members to work for a competing business while working with us. Please allow as much notice as possible if you take a job with a competing business.

### Another corporate job

As a startup ourselves, we understand peoples' motivations to start their own businesses. We applaud you for that. However, we believe that taking on a second job (i.e. as an employee, not as a partner or founder) is a distraction for the team member and can create legal and practical conflicts between the two businesses.

If you're looking to take another corporate job for the money, then there is likely a deeper issue at play. We should discuss that right away.

## Your contract

Generally, we provide employment agreements and contracts with very strict terms stating you cannot work for or on any businesses outside of work. As long as you follow the guidelines above, generally we will override that rule to allow you to work on a side business. As with much of our business, we expect both sides to be reasonable.

Failure to disclose another business without the chance for both parties to review the competitive nature is seen as a severe breach of trust and misalignment with our core values.

---
name: Your Name
description: A short description (only 40 characters is displayed on your card on the [team member's page](/peopleops/team_members/).)
position: MVP
photo: "https://res.cloudinary.com/ha7syshrr/image/upload/v1540269999/gtx12imoelxrx95ghijz.jpg"
HomeBase:
LinkedIn:
Twitter:
Facebook:
GitHub:
Bitbucket:
GitLab:
Instagram:
WebsiteURL:
---

<TeamMemberBreadcrumb :name="$page.frontmatter.name"/>

# {{ $page.frontmatter.name }}

It is recommended that all team members complete a bio here. Feel free to complete as much or as little information as you like.

## Communication Preferences

Do you have communication preferences that we should know about?

## Scheduling Preferences

What's the best way for team members schedule time with you?

### Scheduling Link

If you have a scheduling link (e.g. Calendly) add it here.

## Location

What time zone(s) do you spend your time in?

## Working Hours

Do you have a preference for when you usually work? If not, that's okay, but information here helps team members schedule time for synchronous communication, when required.

## Shortcomings

No judgements, what do you think you can improve on and how can we help?

---
name: G. Crawford Leeds
description: At your service.
position: Founder
photo: "/img/team_members/Crawford_Leeds.jpg"
HomeBase: Denver, CO
LinkedIn: crawfordleeds
Twitter: crawfordleeds
Facebook:
GitHub: crawfordleeds
Bitbucket: crawfordleeds
GitLab: crawfordleeds
Instagram: crawfordleeds
WebsiteURL:
---

<TeamMemberBreadcrumb :name="$page.frontmatter.name"/>

# {{ $page.frontmatter.name }}

Hi, I'm G. Crawford Leeds. The "G" stands for Gregory but I go by Crawford. I was born in Singapore in 1993. Since then, I have lived in Jakarta, Toronto, Hong Kong, Rhode Island, Seattle, Boulder (Colorado), and Denver.

## Communication Preferences

* When messaging me, please get right to the point. Don't start with just "Hi." or "Are you free?" as I need more information to make a decision on whether I can and should respond right away. I realize this is, in some cases, a cultural difference, but please note this preference and do not worry about coming off as rude for being direct. I appreciate the efficiency for the company.

* Slack > Email > Texting

* When forwarding email, please add your own context. E.g. "Received this message from _____ and requires you to follow up on A, B, and C." Forwarding messages to me without context is an inefficient use of the company's time, and runs the risk of me missing critical information.

* I like video chats and I prefer when other team members also turn on the video feed. No one's judging you for wearing pajamas, that's one reason why we're remote.

* Err on the side of over-communication. I'd rather receive too much information that too little. When issues arrive, my instincts are to try and help. If you prefer not to receive help, that's fine, I know there are times where the most efficient thing to do is just keep at it. Just let me know.

* Be direct, especially if you find yourself perturbed by me or any of my decisions. It's very possible I didn't even realize I did anything wrong and would like to understand and correct my behavior. I want to make sure everyone is comfortable speaking to me directly, so I don't get angry over mistakes, challenges, issues, even failures as I find it unproductive.

* Please do not make overt displays of frustration over challenges. It's okay to be frustrated while in the weeds of work, but exacerbated sighs of frustration and continued harping on issues is unproductive and demotivating to all.

## Scheduling Preferences

I try to maintain a [maker's schedule](http://www.paulgraham.com/makersschedule.html) in order to engage in deep work. However, I am regularly on Slack for unstructured communication. Message me anytime, and Slack (not email or texting) is the best way to get a quick response. I'm happy to do off the cuff calls/video chats, when practical.

My goal is to prioritize my time in line with the company's objectives. I may be "busy" but if you have something more critical to discuss, we should prioritize your items.

### Scheduling Link

You can schedule time with me [here](https://calendly.com/crawfordleeds/30min). In order to maintain a maker's schedule, this includes somewhat limited availability. If none of these times work you can message me directly.

## Location

**Primary:** U.S. Mountain Time (Nov 7 - March 9: **UTC-7**, March 10 - Nov 6: **UTC-6**)

**Secondary**: U.S. Pacific Time (Nov 7 - March 9: **UTC-8**, March 10 - Nov 6: **UTC-7**)

I spend most of my time in Denver, CO. I also spend time in Seattle, WA and like to recharge by visiting new locations around the world.

## Working Hours

Wherever I am, I tend to work close to "normal" business hours, leaning a bit later than standard. Usually I start around 10 AM. I'm available by Slack at just about every non-sleeping hour of the day, but I will restrict availability by call and video as I find calling/video to be much more mentally draining than messaging.

## Shortcomings

* I am extremely analytical. I think this is overall a positive, but recognize it can be off putting.
* I love to debate. I was kicked out of an ethics class in University for debating both sides of an argument. Now let me tell you why the professor was wrong. Kidding....
* Saying "Uhm" during a pause. It drives me crazy that I say it and I'm trying to stop.
* Sending long emails. Sometimes I get carried away with content in an email. I know overly long emails are a waste of time to write if no one reads it so I'm working on making my ideas more concise.

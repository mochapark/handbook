# Introduction

![Handbook Logo](/img/logo.jpg)

Welcome to the Mocha Park Team Handbook, the comprehensive guide to working at Mocha Park.

The original creation and structure of this handbook was inspired by many companies, but a special thanks is due to GitLab and Zapier which have both been pioneers in creating a public, remote organization.

The goal of this handbook is to provide an independent authority on working at Mocha Park to facilitate remote, asynchronous work.

We expect all team members to make use of and contribute to the handbook. Howevever, it is important to keep in mind that this handbook is a means to an end (delivering on our [mission](/general/mission)), not the end itself. Too much focus on the handbook runs the risk of making our nimble company too bureaucratic. Bear that in mind as you use and contribute to the handbook.

### New Team Members

New team members, please see our [on-boarding guide](/peopleops/onboarding).

### Prospective Team Members

Feel free to take a look around and ask questions at careers@mochapark.com. This is not your ordinary corporate handbook that you sign and forget about. If you like the concept, and like the content, you should consider working with us.
